package dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionPool{
	public static Connection doConnect() throws NamingException, SQLException {
		Context initCon = new InitialContext();

		DataSource ds = (DataSource) initCon.lookup("java:comp/env/dataSourceForPool");
		return ds.getConnection();
	}


}
