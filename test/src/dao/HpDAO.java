package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

public class HpDAO {
	private final String DRIVER_NAME ="com.mysql.jdbc.Driver";


	public int findMyHp() {
		Connection conn =null;
		int result = 0;
		try {
			Class.forName(DRIVER_NAME);
			conn = ConnectionPool.doConnect();
			String sql = "SELECT myhp FROM hpdata_j where 1";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
				result = rs.getInt("myhp");
			}

			rs.close();

			return result;
		} catch (ClassNotFoundException | SQLException | NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return 0;
		}finally {
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}

	}

	public int findEnemyHp() {
		Connection conn =null;
		int result = 0;
		try {
			Class.forName(DRIVER_NAME);
			conn = ConnectionPool.doConnect();
			String sql ="SELECT enemyhp from hpdata_j where 1";
			PreparedStatement pStmt =conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
			result = rs.getInt("enemyhp");
			}

			rs.close();

			return result;
		} catch (ClassNotFoundException | SQLException | NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return 0;
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}

	}

	public void updateMyhp(int myHp) {
		Connection conn = null;

		try {
			Class.forName(DRIVER_NAME);
			conn =ConnectionPool.doConnect();
			String sql ="UPDATE hpdata_j SET myhp=? WHERE 1";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, myHp);
		    pStmt.executeUpdate();
		} catch (ClassNotFoundException | SQLException | NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}

	}

	public void updateEnemyhp(int enemyHp) {
		Connection conn = null;

		try {
			Class.forName(DRIVER_NAME);
			conn = ConnectionPool.doConnect();
			String sql ="UPDATE hpdata_j SET enemyhp=? WHERE 1 ";
			PreparedStatement pStmt =conn.prepareStatement(sql);
			pStmt.setInt(1, enemyHp);
			pStmt.executeUpdate();
		} catch (ClassNotFoundException | SQLException | NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}

	}
}
