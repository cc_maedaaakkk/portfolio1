package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.EnemyRandom;

/**
 * Servlet implementation class Enemy
 */
@WebServlet("/enemy")
public class Enemy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnemyRandom er = new EnemyRandom();
		int r=er.random();

		switch(r){
		case 0:
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/enemy01.jsp");
			dispatcher.forward(request, response);
			break;
		case 1:
			RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/enemy02.jsp");
			dispatcher2.forward(request, response);
			break;
		case 2:
			RequestDispatcher dispatcher3 = request.getRequestDispatcher("/WEB-INF/jsp/enemy03.jsp");
			dispatcher3.forward(request, response);
			break;
		}
	}

}
