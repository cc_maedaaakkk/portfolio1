package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.HpDAO;
import logic.CastInt;

/**
 * Servlet implementation class Main2
 */
@WebServlet("/main2")
public class Main2 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws NullPointerException, ServletException, IOException {



			HpDAO hpdao = new HpDAO();
			int myhp = hpdao.findMyHp();
			int enemyhp = hpdao.findEnemyHp();
			String result = request.getParameter("result");
			CastInt castint = new CastInt();
			castint.cast(result, myhp, enemyhp);

			HttpSession session = request.getSession();
			String secondMyKey=(String) session.getAttribute("secondMyKey");

			switch (secondMyKey) {
			case "gu":

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/first_action.jsp");
				dispatcher.forward(request, response);
				break;
			case "choki":

				RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/second_action.jsp");
				dispatcher2.forward(request, response);
				break;
			case "pa":

				RequestDispatcher dispatcher3 = request.getRequestDispatcher("/WEB-INF/jsp/third_action.jsp");
				dispatcher3.forward(request, response);
				break;
			}


	}


}
