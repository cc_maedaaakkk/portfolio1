package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HpDAO;

/**
 * Servlet implementation class SpAttack
 */
@WebServlet("/SpAttack")
public class SpAttack extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HpDAO dao = new HpDAO();
		int myHp =dao.findMyHp();
		int enemyHp =dao.findEnemyHp();
		int r = (int) Math.floor(Math.random()*2);
		if(r ==1) {
			myHp-=1;
		}else {
			enemyHp-=1;
		}
		dao.updateMyhp(myHp);
		dao.updateEnemyhp(enemyHp);

		RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/sp_atk_result.jsp?id="+r);
		dispatcher.forward(request, response);
	}

}
