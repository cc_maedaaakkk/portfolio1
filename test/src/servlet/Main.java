package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.HpDAO;

/**
 * Servlet implementation class Main
 */
@WebServlet("/main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HpDAO hpdao =new HpDAO();
		hpdao.updateMyhp(3);
		hpdao.updateEnemyhp(3);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/enemy00.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String firstMyKey = request.getParameter("1st");
		String secondMyKey = request.getParameter("2nd");


		HttpSession session = request.getSession();
		session.setAttribute("firstMyKey", firstMyKey);
		session.setAttribute("secondMyKey", secondMyKey);

	switch (firstMyKey) {
			case "gu":
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/first_action.jsp");
				dispatcher.forward(request, response);
				break;
			case "choki":
				RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/second_action.jsp");
				dispatcher2.forward(request, response);
				break;
			case "pa":
				RequestDispatcher dispatcher3 = request.getRequestDispatcher("/WEB-INF/jsp/third_action.jsp");
				dispatcher3.forward(request, response);
				break;
			}
	}
}
