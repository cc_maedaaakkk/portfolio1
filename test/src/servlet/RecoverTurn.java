package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HpDAO;

/**
 * Servlet implementation class RecoverTurn
 */
@WebServlet("/RecoverTurn")
public class RecoverTurn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HpDAO dao = new HpDAO();
		int myHp =dao.findMyHp();
		int r = (int) Math.floor(Math.random()*2);

		if(r ==1) {
			myHp-=1;
		}else {
			myHp+=1;
		}
		dao.updateMyhp(myHp);

		RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/recover_turn_result.jsp?id="+r);
		dispatcher.forward(request, response);
	}



}
