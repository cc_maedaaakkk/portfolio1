package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HpDAO;
import logic.CastInt;

/**
 * Servlet implementation class Main3
 */
@WebServlet("/main3")
public class Main3 extends HttpServlet {
	private static final long serialVersionUID = 1L;



	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String result2 = request.getParameter("result2");

		HpDAO hpdao = new HpDAO();
		int myhp = hpdao.findMyHp();
		int enemyhp = hpdao.findEnemyHp();

		CastInt castint = new CastInt();
		castint.cast(result2, myhp, enemyhp);

		int judge1 = hpdao.findMyHp();
		int judge2 = hpdao.findEnemyHp();

		if (judge1 <= 0 || judge2 <= 0) {
			if (judge1 <= 0 && judge2 <= 0) {
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle_result_drow.jsp");
				dispatcher.forward(request, response);
			} else if (judge2 <= 0) {
				RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/battle_result_win.jsp");
				dispatcher2.forward(request, response);
			} else {
				RequestDispatcher diapatcher3 = request.getRequestDispatcher("/WEB-INF/jsp/battle_result_lose.jsp");
				diapatcher3.forward(request, response);
			}
		}else{
			RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/battle_return.jsp");
			dispatcher.forward(request, response);
		}
	}

}
