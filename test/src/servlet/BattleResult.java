package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.JankenAlgolism;

/**
 * Servlet implementation class BattleResult
 */
@WebServlet("/battle_result")
public class BattleResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws NullPointerException, ServletException, IOException {
		String enemyKey = request.getParameter("enemyKey");
		JankenAlgolism jalg = new JankenAlgolism();

		HttpSession session = request.getSession();

		String firstMyKey = (String) session.getAttribute("firstMyKey");


		if(firstMyKey!=null) {
			int result = jalg.calcAlgo(firstMyKey, enemyKey);

			switch (result) {
			case 0:
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/result_drow.jsp");
				dispatcher.forward(request, response);
				break;
			case 1:
				RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/result_lose.jsp");
				dispatcher1.forward(request, response);
				break;
			case 2:
				RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/result_win.jsp");
				dispatcher2.forward(request, response);
				break;
			}

			session.removeAttribute("firstMyKey");

		}else {

			String secondMyKey= (String) session.getAttribute("secondMyKey");

			int result = jalg.calcAlgo(secondMyKey, enemyKey);

			switch (result) {
			case 0:
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/result_drow2.jsp");
				dispatcher.forward(request, response);

				break;
			case 1:
				RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/result_lose2.jsp");
				dispatcher1.forward(request, response);

				break;
			case 2:
				RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/result_win2.jsp");
				dispatcher2.forward(request, response);

				break;
			}
			session.removeAttribute("secondMyKey");
		}
	}

}
