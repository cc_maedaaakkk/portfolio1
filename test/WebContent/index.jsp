<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@include file ="/WEB-INF/include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <title>スタートページ</title>
    <style>
    body{
        background-color: orange;
    }
    h1{
        text-align: center;
    }

    form{
        display: flex;
        justify-content: center;
        background-color: black;
        height: 500px;
        margin-top: 5rem;
        padding: 20%;
    }
    button{
        height: 250px;
        width: 350px;
        background-color: greenyellow;
        font-size: 3rem;
        font-weight: bold;
        transition: 2s;
        border-radius: 50%;
    }
    button:hover{
        animation: pulse 1.5s infinite;
        box-shadow: 0 0 0 50rem rgba(223, 211, 211, 0);
        border-color: greenyellow;
        color: aliceblue;
    }

    @keyframes pulse{
        100%{
        box-shadow: 0 0 7 50rem greenyellow;
        }
        0%{
            box-shadow: 0 0 0 0 greenyellow;
        }
    }

    </style>
</head>
<body>
	<a href="http://koukimaeda.1strentalserver.info/php/portfolio01/">PHP版へ(内容は同じです。。)</a>
    <h1>ロボプラバトルシュミレータ</h1>


   <form action="${sitePath}/main" method="GET">
       <button type="submit">＞索敵＜</button>
    </form>

</body>
</html>