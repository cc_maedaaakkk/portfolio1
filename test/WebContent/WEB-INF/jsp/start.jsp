<%@page import="dao.HpDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="dao.HpDAO" %>
	<%
	HpDAO hpdao = new HpDAO();
	int myhp=hpdao.findMyHp();
	int enemyhp=hpdao.findEnemyHp();
	%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="firstAction">GNブレードアタック</c:set>
<c:set var="secondAction">ビームサーベル連撃</c:set>
<c:set var="thirdAction">トランザムバースト</c:set>
<%@include file ="/WEB-INF/include/common.jsp" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>行動選択画面</title>
<link rel="stylesheet" href="${sitePath}/css/start.css">
</head>
<body>
<h1>「ガンダムエクシア　トランザムモード」</h1>
<img src="${sitePath}/img/001.JPG" alt="自機の写真" />
<p class="stock">自機の体力ストック：残り<span><%=myhp %></span>・敵の体力ストック：残り<span><%=enemyhp%></span></p>
<div class="circle">
	<p class="ele-title">ーー属性表ーー</p>
	<div class="elements-top"><p class="gu">G</p></div>
	<div class="marker">↗　↘</div>
	<div class="elements">P</div>
	<div class="marker2">←</div>
	<div class="elements">C</div>
</div>
<form action="/test/main" method="post">
<div class="command"><p class="subtitle">1stbattle</p><br>
	<select name="1st" id="">
	<option value="gu">${firstAction}:G</option>
	<option value="choki">${secondAction}:C</option>
	<option value="pa">${thirdAction}:P</option>
	</select>

</div>
<div class="command"><p class="subtitle">2ndbattle</p><br>
	<select name="2nd" id="">
		<option value="gu">${firstAction}:G</option>
		<option value="choki">${secondAction}:C</option>
		<option value="pa">${thirdAction}:P</option>
	</select>

</div>

<button type="submit">戦闘開始</button>
</form>


</body>
</html>