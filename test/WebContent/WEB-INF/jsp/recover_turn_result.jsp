<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@page import="dao.HpDAO" %>

      <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
      <%
      String palam =request.getParameter("id");
      int palamInt = Integer.parseInt(palam);
      HpDAO dao = new HpDAO();
      int myhp = dao.findMyHp();
      int enemyhp = dao.findEnemyHp();

      %>
      <c:set var="palamInt" value="<%=palamInt %>"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>回避行動結果</title>
</head>
<body>
<c:choose>
	<c:when test="${palamInt==1}">
	<h1>回避行動失敗・・・ダメージ！！</h1>
	<p>自機残り体力ストック：<%=myhp %></p>
	<p>敵機残り体力ストック：<%=enemyhp %></p>
	</c:when>
	<c:otherwise>
	<h1>回避行動成功！！！体力回復</h1>
	<p>自機残り体力ストック：<%=myhp %></p>
	<p>敵機残り体力ストック：<%=enemyhp %></p>
	</c:otherwise>
</c:choose>

<form action="/test/battle_start" method="get">
<button type="submit">次へ</button>
</form>
</body>
</html>