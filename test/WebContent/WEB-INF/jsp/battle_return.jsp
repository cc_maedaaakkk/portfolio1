<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="dao.HpDAO" %>
	<%
	HpDAO hpdao = new HpDAO();
	int myhp=hpdao.findMyHp();
	int enemyhp=hpdao.findEnemyHp();
	%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file ="/WEB-INF/include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>return</title>
<link rel="stylesheet" href="${sitePath}/css/battlereturn.css">
</head>
<body>
<p>自機残り体力ストック：<span><%=myhp %></span>／敵機残り体力ストック：<span><%=enemyhp %></span></p>
    <h1>戦闘継続</h1>

    <form action="/test/SpAttack" method="get">
    	<button type="submit" class="sp">急襲</button>
    	<div class="hide">
    	※５０％の確率で敵機に１ダメージ。<br>
    	失敗すると自機に１ダメージ。
    	</div>
    </form>
    <form action="/test/RecoverTurn" method="get">
    	<button type="submit" class="sp">回避行動</button>
    	<div class="hide">
    	※５０％の確率で自機の体力１回復。<br>
    	失敗すると自機に１ダメージ。
    	</div>
    </form>
<form action="/test/battle_start" method="get">
<button type="submit">行動選択へ</button>
</form>
</body>
</html>